module EventsHelper
  def within(event)
    now = DateTime.now
    result = 'default'
    # login on time
    if event.login && (event.start + 15.minutes) > event.login && event.login > (event.start - 15.minutes)
      result = 'success'

      # logout on time
      if event.logout && (event.stop + 15.minutes) > event.logout && event.logout > (event.stop - 15.minutes)
        result = 'success'
        # logged out but late
      elsif event.logout && !((event.stop + 15.minutes) > event.logout && event.logout > (event.stop - 15.minutes))
        result = 'danger'
      # not logged out and safe to
      elsif event.logout.nil? && (event.stop + 15.minutes) > now && now > (event.stop - 15.minutes)
        result = 'warning'
      # not logged out and late
      elsif event.logout.nil? && (event.stop + 15.minutes) < now
        result = 'danger'
      end
      # logged in but late
    elsif event.login && !((event.start + 15.minutes) > event.login && event.login > (event.start - 15.minutes))
      result = 'danger'
    # not logged in and safe to
    elsif event.login.nil? && (event.start + 15.minutes) > now && now > (event.start - 15.minutes)
      result = 'warning'
    # not logged in and late
    elsif event.login.nil? && (event.start + 15.minutes) < now
      result = 'danger'
    end

    result
  end


  def time_diff(start_time, end_time)
    seconds_diff = (start_time - end_time).to_i.abs

    hours = seconds_diff / 3600
    seconds_diff -= hours * 3600

    minutes = seconds_diff / 60
    #seconds_diff -= minutes * 60

    #seconds = seconds_diff

    "#{hours.to_s} hr #{minutes.to_s} min"
  end
end
