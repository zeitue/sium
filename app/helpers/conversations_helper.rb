module ConversationsHelper
  def mailbox_section(title, current_box, opts = {})
    opts[:class] = opts.fetch(:class, '')
    opts[:class] += ' active' if title.downcase == current_box
    content_tag :li, link_to(('<span>&nbsp;&nbsp;&nbsp;'+glyph("#{title.to_s == 'sent' ? 'send' : title}".to_sym) + '&nbsp;&nbsp;&nbsp;</span>').html_safe, conversations_path(box: title.downcase), class: "btn btn-lg"), opts
  end
end
