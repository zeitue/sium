module SkillsHelper
  def overseer_options
    s = ''
    s << '<option selected disabled style="display:none"></option>'
    User.where.not(account_type: 3).each do |user|
      s << "<option value='#{user.id}' #{user.identifier}>#{user.identifier}</option>"
    end
    s.html_safe
  end
end
