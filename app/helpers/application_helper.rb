module ApplicationHelper
  # ==== Examples
  # glyph(:share_alt)
  # # => <i class="icon-share-alt"></i>
  # glyph(:lock, :white)
  # # => <i class="icon-lock icon-white"></i>

  def glyph(*names)
    content_tag :i, nil, class: names.map{|name| "glyphicon glyphicon-#{name.to_s.gsub('_','-')}" }
  end

  def fa(*names)
    content_tag :i, nil, class: names.map{|name| "fa fa-#{name.to_s.gsub('_','-')}" }
  end

  def page_header(text)
    content_for(:page_header) { text.to_s }
  end
end
