module ContactsHelper
  def contact_link(contact)
    case contact.type_field
    # email address
    when 0
      mail_to(contact.field, glyph(:envelope) + ' ' +  contact.field.to_s, :class => 'btn btn-lg btn-primary btn-block')
    # phone number
    when 1
      tel_to(contact.field, :phone)
    when 2
      tel_to(contact.field, :earphone)
    when 3
      skype_to(contact.field)
    when 4
      fax_to(contact.field)
    end
  end

  def skype_to(text)
    link_to (fa(:skype) + ' ' + text), "skype:#{text}?chat", :class => 'btn btn-lg btn-primary btn-block'
  end
  def tel_to(text, glyphicon)
    groups = text.to_s.scan(/(?:^\+)?\d+/)
    if groups.size > 1 && groups[0][0] == '+'
      # remove leading 0 in area code if this is an international number
      groups[1] = groups[1][1..-1] if groups[1][0] == '0'
      groups.delete_at(1) if groups[1].size == 0 # remove if it was only a 0
    end
    link_to glyph(glyphicon) + ' ' + text, "tel:#{groups.join '-'}", :class => 'btn btn-lg btn-primary btn-block'
  end

    def fax_to(text)
    groups = text.to_s.scan(/(?:^\+)?\d+/)
    if groups.size > 1 && groups[0][0] == '+'
      # remove leading 0 in area code if this is an international number
      groups[1] = groups[1][1..-1] if groups[1][0] == '0'
      groups.delete_at(1) if groups[1].size == 0 # remove if it was only a 0
    end
    link_to fa(:fax) + ' ' + text, "fax:#{groups.join '-'}", :class => 'btn btn-lg btn-primary btn-block'
  end

  def service_glyphicon(contact)
    case contact.type_field
    when 0
      glyph(:envelope)
    when 1
      glyph(:phone)
    when 2
      glyph(:earphone)
    when 3
      fa(:skype)
    when 4
      fa(:fax)
    end
  end

end
