module MessagesHelper
  def recipients_options(chosen_recipient = nil)
    s = ''
    s << '<option selected disabled style="display:none"></option>'
    User.all.each do |user|
      s << "<option value='#{user.id}' #{user.identifier}' #{'selected' if user == chosen_recipient}>#{user.identifier}</option>"
    end
    s.html_safe
  end
end
