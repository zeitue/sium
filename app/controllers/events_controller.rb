# coding: utf-8
class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy, :login, :logout, :login_update, :logout_update]
  before_action :has_power?, only: [:new, :show, :edit, :update, :destroy]
  before_action :define_time_courses, only: [:new, :edit, :update, :create]

  def list
    if params[:day] && Date.valid_date?(params[:year].to_i, params[:month].to_i, params[:day].to_i)
      @current = Date.new(params[:year].to_i, params[:month].to_i, params[:day].to_i)
      @older = @current - 1.day
      @newer = @current + 1.day
      if current_user.has_power?
        @events = Event.where("cast(extract(year from start) as int) = ? and cast(extract(month from start) as int) = ? and cast(extract(day from start) as int) = ?", @current.year, @current.month, @current.day).group_by{ |item| item.start.to_date }
      else
        @events = Event.where("(student_id LIKE #{current_user.id} OR overseer_id LIKE #{current_user.id}) and cast(extract(year from start) as int) = ? and cast(extract(month from start) as int) = ? and cast(extract(day from start) as int) = ?", @current.year, @current.month, @current.day).group_by{ |item| item.start.to_date }
      end
    elsif params[:month] && Date.valid_date?(params[:year].to_i, params[:month].to_i,1)
      @current = Date.new(params[:year].to_i, params[:month].to_i)
      @older = @current - 1.month
      @newer = @current + 1.month
      if current_user.has_power?
        @events = Event.where("cast(extract(year from start) as int) = ? and cast(extract(month from start) as int) = ?", @current.year, @current.month).group_by{ |item| item.start.to_date }
      else
        @events = Event.where("(student_id LIKE #{current_user.id} OR overseer_id LIKE #{current_user.id}) and cast(extract(year from start) as int) = ? and cast(extract(month from start) as int) = ?", @current.year, @current.month).group_by{ |item| item.start.to_date }
      end
    elsif params[:year] && !params[:month] && Date.valid_date?(params[:year].to_i, 1,1)
      @current = Date.new(params[:year].to_i)
      @older = @current - 1.year
      @newer = @current + 1.year
      if current_user.has_power?
        @events = Event.where("cast(extract(year from start) as int) = ?", @current.year).group_by{ |item| item.start.to_date }
      else
        @events = Event.where("(student_id LIKE #{current_user.id} OR overseer_id LIKE #{current_user.id}) and cast(extract(year from start) as int) = ?", @current.year).group_by{ |item| item.start.to_date }
      end
    else
      @current = Date.today
      @older = @current - 1.month
      @newer = @current + 1.month
       if current_user.has_power?
         @events = Event.all.group_by{ |item| item.start.to_date }
       else
         @events = Event.where("student_id LIKE #{current_user.id} OR overseer_id LIKE #{current_user.id}").group_by{ |item| item.start.to_date }
       end
    end

  end

  def calendar
    if current_user.has_power?
      @events = Event.all.group_by{ |item| item.start.to_date }
    else
      @events = Event.where("student_id LIKE #{current_user.id} OR overseer_id LIKE #{current_user.id}").group_by{ |item| item.start.to_date }
    end

    if params[:month] && Date.valid_date?(params[:year].to_i, params[:month].to_i,1)
      @current = Date.new(params[:year].to_i, params[:month].to_i)
    else
      @current = Date.today
    end
    @older = @current - 1.month
    @newer = @current + 1.month
  end

  def master_clinical_log
    @master_clinical_logs = Event.all.page(params[:page]).per(16)
  end

  def todo
    if current_user.has_power?
      @events = Event.where("(login IS NULL OR logout IS NULL) OR stop > ?", DateTime.now).order('start DESC')
    else
      @events = Event.where("(student_id LIKE #{current_user.id} OR overseer_id LIKE #{current_user.id}) AND ((login IS NULL OR logout IS NULL) OR stop > ?)", DateTime.now).order('start DESC')
    end
  end


  def login
  end

  def login_update
    if (@event.overseer.pin.to_i == params[:pin].to_i)
      @event.update(event_params)
      redirect_to events_list_day_path(@event.start.year,@event.start.month, @event.start.day), notice: 'Event, login successful'
    else
      respond_to do |format|
        format.html {
          flash[:alert] = 'Invalid Pin'
          render :login
        }
      end
    end
  end

  def logout
  end

  def logout_update
    if (@event.overseer.pin.to_i == params[:pin].to_i)
      @event.update(event_params)
      redirect_to events_list_day_path(@event.start.year,@event.start.month, @event.start.day), notice: 'Event logout, successful'
    else
      respond_to do |format|
        format.html {
          flash[:alert] = 'Invalid Pin'
          render :logout
        }
      end
    end
  end

  # GET /events
  def index
    redirect_to event_calendar_path
  end

  # GET /events/1
  def show
  end

  # GET /events/new
  def new
    @event = Event.new
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events
  def create
    @event = Event.new(event_params)

    if @event.save
      redirect_to events_list_day_path(@event.start.year,@event.start.month, @event.start.day), notice: 'Event was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /events/1
  def update
    if @event.update(event_params)
      redirect_to events_list_day_path(@event.start.year,@event.start.month, @event.start.day), notice: 'Event was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /events/1
  def destroy
    @event.destroy
    redirect_to event_calendar_path, notice: 'Event was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def event_params
      params.require(:event).permit(:overseer_id, :student_id, :course_id, :start, :stop, :login, :logout, :location_id)
    end

    def define_time_courses
      @time_courses = TimeCourse.all.inject({}) do |options, course|
        (options[ course.course.identifier] ||= []) << [course.identifier, course.id]
        options
      end
    end
end
