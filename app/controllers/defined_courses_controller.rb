class DefinedCoursesController < ApplicationController
  before_action :set_defined_course, only: [:show, :edit, :update, :destroy]

  # GET /defined_courses
  def index
    @defined_courses = DefinedCourse.all.page(params[:page]).per(16)
  end

  # GET /defined_courses/1
  def show
  end

  # GET /defined_courses/new
  def new
    @defined_course = DefinedCourse.new
  end

  # GET /defined_courses/1/edit
  def edit
  end

  # POST /defined_courses
  def create
    @defined_course = DefinedCourse.new(defined_course_params)

    if @defined_course.save
      redirect_to @defined_course, notice: 'Defined course was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /defined_courses/1
  def update
    if @defined_course.update(defined_course_params)
      redirect_to @defined_course, notice: 'Defined course was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /defined_courses/1
  def destroy
    @defined_course.destroy
    redirect_to defined_courses_url, notice: 'Defined course was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_defined_course
      @defined_course = DefinedCourse.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def defined_course_params
      params.require(:defined_course).permit(:name, :number, :level)
    end
end
