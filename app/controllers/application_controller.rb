class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  before_action :set_locale

  def set_locale
    I18n.locale = (params[:locale] unless params[:locale].nil? || params[:locale].empty?) || (current_user.language unless current_user.nil?) || I18n.default_locale
  end

  def check_ownership(object)
    unless (current_user.id == object.user_id)
      redirect_to authenticated_root_path, alert: "Operation failed: Permission denied"
    end
  end

  private

  def after_sign_out_path_for(resource_or_scope)
    unauthenticated_root_path(:locale => params[:locale])
  end

  def has_power?
    unless current_user.has_power?
      redirect_to authenticated_root_path, alert: "Operation failed: Permission denied"
    end
  end

  rescue_from ActiveRecord::RecordNotFound do
    flash[:warning] = 'Resource not found.'
    redirect_back_or authenticated_root_path
  end

  def redirect_back_or(path)
    redirect_to request.referer || path
  end
=begin
  private
  def authenticate_user!
    if user_signed_in?
      super
    else
      redirect_to unauthenticated_root_path, notice: "Please Sign in to use SIUM" if !devise_controller?
    end
  end
=end
end
