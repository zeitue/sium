class LocationsController < ApplicationController
  before_action :set_location, only: [:show, :edit, :update, :destroy]

  def autocomplete
    term = params[:term]
    locations = Location.where("(name || ' @ ' || address_line1 || ' ' ||\
                                 country || ' ' || city || ', ' || state ||\
                                 ' ' || zip || ', ' || clinical_site || ', ' ||\
                                 unit) LIKE '%#{term}%' OR\
                                 (name || ' @ ' || address_line1 || ' ' ||\
                                 country || ' ' || city || ', ' || state ||\
                                 ' ' || zip || ', ' || unit) LIKE '%#{term}%' OR\
                                 (name || ' @ ' || address_line2 || ' ' ||\
                                 country || ' ' || city || ', ' || state ||\
                                 ' ' || zip || ', ' || clinical_site || ', ' ||\
                                 unit) LIKE '%#{term}%' OR\
                                 (name || ' @ ' || address_line2 || ' ' ||\
                                 country || ' ' || city || ', ' || state ||\
                                 ' ' || zip || ', ' || unit) LIKE '%#{term}%'")
    render json: locations.map { |location|
      { id: location.id,
        label: location.identifier,
        value: location.identifier } }
  end


  # GET /locations
  def index
    @locations = Location.all.page(params[:page]).per(16)
  end

  # GET /locations/1
  def show
  end

  # GET /locations/new
  def new
    @location = Location.new
  end

  # GET /locations/1/edit
  def edit
  end

  # POST /locations
  def create
    @location = Location.new(location_params)

    if @location.save
      redirect_to locations_path, notice: 'Location was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /locations/1
  def update
    if @location.update(location_params)
      redirect_to locations_path, notice: 'Location was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /locations/1
  def destroy
    @location.destroy
    redirect_to locations_url, notice: 'Location was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_location
      @location = Location.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def location_params
      params.require(:location).permit(:name, :address_line1, :address_line2, :country, :state, :city, :zip, :clinical_site, :unit)
    end
end
