# coding: utf-8
class SkillsController < ApplicationController
  before_action :set_skill, only: [:show, :edit, :update, :destroy, :level_up, :level_up_update]
  before_action :define_skills, except: [:index, :destroy, :update]
  # GET /skills
  def index
    @skills = current_user.skills.page(params[:page]).per(16)
  end

  # GET /skills/1
  def show
  end

  # GET /skills/new
  def new
    @skill = Skill.new
  end

  # GET /skills/1/edit
  def edit
  end

  def level_up
  end

  def level_up_update
    puts params
    if params[:overseer]
      @user = User.find(params[:overseer].to_i)
    end

    if (!@user.nil? && (@user.pin.to_i == params[:pin].to_i))
      @skill.update_attribute(:level, @skill.level+1)
      redirect_to skills_path, notice: 'Skill, leveled up'
    else
      respond_to do |format|
        format.html {
          flash[:alert] = 'Invalid Pin'
          render :level_up
        }
      end
    end
  end

  # POST /skills
  def create
    @skill = Skill.new(skill_params)

    if @skill.save
      redirect_to skills_path, notice: 'Skill was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /skills/1
  def update
    if @skill.update(skill_params)
      redirect_to skills_path, notice: 'Skill was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /skills/1
  def destroy
    @skill.destroy
    redirect_to skills_url, notice: 'Skill was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_skill
      @skill = Skill.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def skill_params
      params.require(:skill).permit(:user_id, :defined_skill_id, :level)
    end

    def define_skills
      @defined_skills = DefinedSkill.all.inject({}) do |options, skill|
      (options['• ' + skill.category] ||= []) << [skill.name, skill.id]
      options
    end
    end
end
