class RegistrationsController < Devise::RegistrationsController

  private

  def sign_up_params
    params.require(:user).permit(:prefix, :first_name, :middle_name, :last_name, :suffix, :pin, :level, :account_type, :privacy, :home_unit, :email, :password, :password_confirmation, :language)
  end

  def account_update_params
    params.require(:user).permit(:prefix, :first_name, :middle_name, :last_name, :suffix, :pin, :level, :account_type, :privacy, :home_unit, :email, :password, :password_confirmation, :current_password, :language)
  end
end
