class OverseerEvaluationFormsController < ApplicationController
  before_action :set_overseer_evaluation_form, only: [:show, :edit, :update, :destroy]

  # GET /overseer_evaluation_forms
  def index
    @overseer_evaluation_forms = OverseerEvaluationForm.all
  end

  # GET /overseer_evaluation_forms/1
  def show
  end

  # GET /overseer_evaluation_forms/new
  def new
    @overseer_evaluation_form = OverseerEvaluationForm.new
  end

  # GET /overseer_evaluation_forms/1/edit
  def edit
  end

  # POST /overseer_evaluation_forms
  def create
    @overseer_evaluation_form = OverseerEvaluationForm.new(overseer_evaluation_form_params)

    if @overseer_evaluation_form.save
      redirect_to root_path, notice: 'Overseer evaluation form was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /overseer_evaluation_forms/1
  def update
    if @overseer_evaluation_form.update(overseer_evaluation_form_params)
      redirect_to root_path, notice: 'Overseer evaluation form was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /overseer_evaluation_forms/1
  def destroy
    @overseer_evaluation_form.destroy
    redirect_to overseer_evaluation_forms_url, notice: 'Overseer evaluation form was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_overseer_evaluation_form
      @overseer_evaluation_form = OverseerEvaluationForm.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def overseer_evaluation_form_params
      params.require(:overseer_evaluation_form).permit(:event_id, :overseer_id, :f0, :f1, :f2, :f3, :f4, :f5, :f6, :f7, :comment)
    end
end
