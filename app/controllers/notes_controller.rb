class NotesController < ApplicationController
  before_action :set_note, only: [:show, :edit, :update, :destroy, :download]

  # GET /notes
  def index
    @notes = current_user.notes.order('updated_at DESC').page(params[:page]).per(8)
  end

  # GET /notes/1
  def show
    check_ownership(@note)
  end

  # GET /notes/new
  def new
    @note = Note.new
  end

  # GET /notes/1/edit
  def edit
    check_ownership(@note)
  end

  def download
    check_ownership(@note)
    body = @note.body
    send_data body, :filename => "#{@note.title}.txt", :disposition => 'attachment'
  end

  # POST /notes
  def create
    @note = Note.new(note_params)
    @note.user_id = current_user.id
    if @note.save
      redirect_to notes_url, notice: 'Note was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /notes/1
  def update
    check_ownership(@note)
    if @note.update(note_params)
      redirect_to notes_url, notice: 'Note was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /notes/1
  def destroy
    check_ownership(@note)
    @note.destroy
    redirect_to notes_url, notice: 'Note was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_note
      @note = Note.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def note_params
      params.require(:note).permit(:title, :body, :user_id, :background_color)
    end
end
