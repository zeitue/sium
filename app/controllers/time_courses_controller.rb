# coding: utf-8
class TimeCoursesController < ApplicationController
  before_action :set_time_course, only: [:show, :edit, :update, :destroy]
  before_action :define_courses, except: [:index, :destroy, :update]
  # GET /time_courses
  def index
    @time_courses = TimeCourse.all.page(params[:page]).per(16)
  end

  # GET /time_courses/1
  def show
  end

  # GET /time_courses/new
  def new
    @time_course = TimeCourse.new
  end

  # GET /time_courses/1/edit
  def edit
  end

  # POST /time_courses
  def create
    @time_course = TimeCourse.new(time_course_params)

    if @time_course.save
      redirect_to @time_course, notice: 'Time course was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /time_courses/1
  def update
    if @time_course.update(time_course_params)
      redirect_to @time_course, notice: 'Time course was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /time_courses/1
  def destroy
    @time_course.destroy
    redirect_to time_courses_url, notice: 'Time course was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_time_course
      @time_course = TimeCourse.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def time_course_params
      params.require(:time_course).permit(:course_id, :instructor_id, :start, :stop)
    end

    def define_courses
      @defined_courses = DefinedCourse.all.inject({}) do |options, course|
        (options['• ' + t('level') + ' ' + course.level.to_s] ||= []) << [course.name.to_s + ' - ' + course.number.to_s, course.id]
        options
      end
    end
end
