class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  respond_to :html, :js

  def autocomplete
    term = params[:term]
    users = User.where("email LIKE '%#{term}%' OR (prefix || ' ' || first_name\
                       || ' ' || middle_name || ' ' || last_name || ' '\
                       || suffix || ' <' || email || '>') LIKE '%#{term}%'
                       OR (first_name || ' ' || last_name) LIKE '%#{term}%'")
    render json: users.map { |user|
      { id: user.id,
        label: user.identifier,
        value: user.identifier } }
  end

  def autocomplete_student
    term = params[:term]
    users = User.where("account_type=3 AND (email LIKE '%#{term}%' OR (prefix || ' ' || first_name\
                       || ' ' || middle_name || ' ' || last_name || ' '\
                       || suffix || ' <' || email || '>') LIKE '%#{term}%'
                       OR (first_name || ' ' || last_name) LIKE '%#{term}%')")
    render json: users.map { |user|
      { id: user.id,
        label: user.identifier,
        value: user.identifier } }
  end

  def autocomplete_overseer
    term = params[:term]
    users = User.where("(account_type=1 OR account_type=2 OR account_type=0) AND (email LIKE '%#{term}%' OR (prefix || ' ' || first_name\
                       || ' ' || middle_name || ' ' || last_name || ' '\
                       || suffix || ' <' || email || '>') LIKE '%#{term}%'
                       OR (first_name || ' ' || last_name) LIKE '%#{term}%')")
    render json: users.map { |user|
      { id: user.id,
        label: user.identifier,
        value: user.identifier } }
  end

  def autocomplete_has_power
    term = params[:term]
    users = User.where("email LIKE '%#{term}%' OR (prefix || ' ' || first_name\
                       || ' ' || middle_name || ' ' || last_name || ' '\
                       || suffix || ' <' || email || '>') LIKE '%#{term}%'
                       OR (first_name || ' ' || last_name) LIKE '%#{term}%'")
    render json: users.map { |user|
      { id: user.id,
        label: user.identifier,
        value: user.identifier } }
  end

    def autocomplete_faculty
    term = params[:term]
    users = User.where("account_type=1 AND (email LIKE '%#{term}%' OR (prefix || ' ' || first_name\
                       || ' ' || middle_name || ' ' || last_name || ' '\
                       || suffix || ' <' || email || '>') LIKE '%#{term}%'
                       OR (first_name || ' ' || last_name) LIKE '%#{term}%')")
    render json: users.map { |user|
      { id: user.id,
        label: user.identifier,
        value: user.identifier } }
    end

  def show
    @user = User.find(params[:id])
  end

  def index
    @users = User.all.page(params[:page]).per(8)
  end

  def edit
  end

  def update
    if @user.update(user_params)
      respond_to do |format|
        format.js {render inline: "location.reload();" }
      end

      #redirect_to @user, notice: 'User was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @user = User.find(params[:id])
    if @user.destroy
      redirect_to users_path, notice: "User deleted."
    end
  end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:prefix, :first_name, :middle_name, :last_name, :suffix, :account_type, :language, :privacy)
    end
end
