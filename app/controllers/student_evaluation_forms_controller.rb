class StudentEvaluationFormsController < ApplicationController
  before_action :set_student_evaluation_form, only: [:show, :edit, :update, :destroy]

  # GET /student_evaluation_forms
  def index
    @student_evaluation_forms = StudentEvaluationForm.all
  end

  # GET /student_evaluation_forms/1
  def show
  end

  # GET /student_evaluation_forms/new
  def new
    @student_evaluation_form = StudentEvaluationForm.new
  end

  # GET /student_evaluation_forms/1/edit
  def edit
  end

  # POST /student_evaluation_forms
  def create
    @student_evaluation_form = StudentEvaluationForm.new(student_evaluation_form_params)

    if @student_evaluation_form.save
      redirect_to root_path, notice: 'Student evaluation form was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /student_evaluation_forms/1
  def update
    if @student_evaluation_form.update(student_evaluation_form_params)
      redirect_to root_path, notice: 'Student evaluation form was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /student_evaluation_forms/1
  def destroy
    @student_evaluation_form.destroy
    redirect_to root_path, notice: 'Student evaluation form was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student_evaluation_form
      @student_evaluation_form = StudentEvaluationForm.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def student_evaluation_form_params
      params.require(:student_evaluation_form).permit(:event_id, :student_id, :f0, :f1, :f2, :f3, :f4, :f5, :f6, :comment)
    end
end
