class DefinedSkillsController < ApplicationController
  before_action :set_defined_skill, only: [:show, :edit, :update, :destroy]

  # GET /defined_skills
  def index
    @defined_skills = DefinedSkill.all.page(params[:page]).per(16)
  end

  # GET /defined_skills/1
  def show
  end

  # GET /defined_skills/new
  def new
    @defined_skill = DefinedSkill.new
  end

  # GET /defined_skills/1/edit
  def edit
  end

  # POST /defined_skills
  def create
    @defined_skill = DefinedSkill.new(defined_skill_params)

    if @defined_skill.save
      redirect_to @defined_skill, notice: 'Defined skill was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /defined_skills/1
  def update
    if @defined_skill.update(defined_skill_params)
      redirect_to @defined_skill, notice: 'Defined skill was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /defined_skills/1
  def destroy
    @defined_skill.destroy
    redirect_to defined_skills_url, notice: 'Defined skill was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_defined_skill
      @defined_skill = DefinedSkill.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def defined_skill_params
      params.require(:defined_skill).permit(:name, :category, :required_level)
    end
end
