class OverseerEvaluationForm < ActiveRecord::Base
  belongs_to :event
  belongs_to :overseer
  belongs_to :overseer, foreign_key: 'overseer_id', class_name: 'User'
  validates_presence_of :f0
  validates_presence_of :f1
  validates_presence_of :f2
  validates_presence_of :f3
  validates_presence_of :f4
  validates_presence_of :f5
  validates_presence_of :f6
  validates_presence_of :f7
  validates_presence_of :overseer
  validates_presence_of :event
  validates_numericality_of :f0, :greater_than_or_equal_to => 1, :less_than_or_equal_to => 4
  validates_numericality_of :f1, :greater_than_or_equal_to => 1, :less_than_or_equal_to => 4
  validates_numericality_of :f2, :greater_than_or_equal_to => 1, :less_than_or_equal_to => 4
  validates_numericality_of :f3, :greater_than_or_equal_to => 1, :less_than_or_equal_to => 4
  validates_numericality_of :f4, :greater_than_or_equal_to => 1, :less_than_or_equal_to => 4
  validates_numericality_of :f5, :greater_than_or_equal_to => 1, :less_than_or_equal_to => 4
  validates_numericality_of :f6, :greater_than_or_equal_to => 1, :less_than_or_equal_to => 4
  validates_numericality_of :f7, :greater_than_or_equal_to => 1, :less_than_or_equal_to => 4
end
