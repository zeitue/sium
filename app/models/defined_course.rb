class DefinedCourse < ActiveRecord::Base
  has_many :time_courses

  def identifier
    buffer = ""
    buffer += self.name.to_s + ' - ' +  self.number.to_s + ' (' + I18n.t('level') + ' ' + self.level.to_s + ')'
    buffer
  end
end
