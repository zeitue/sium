class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :lockable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable
  acts_as_messageable
  validates_presence_of :first_name
  validates_presence_of :last_name
  has_many :notes
  has_many :contacts
  has_many :courses
  has_one :program
  has_many :skills

  def mailboxer_email(object)
    self.email
  end
  def name
    buffer = ""
    buffer += self.prefix.to_s + ' ' unless self.prefix.to_s.empty?
    buffer += self.first_name.to_s + ' ' unless self.first_name.to_s.empty?
    buffer += self.middle_name.to_s + ' ' unless self.middle_name.to_s.empty?
    buffer += self.last_name.to_s unless self.last_name.to_s.empty?
    buffer += ' ' + self.suffix.to_s unless self.suffix.to_s.empty?
    buffer
  end

  def identifier
    self.name + " ["+ self.email.to_s + "] (" + account_type_name + ')'
  end

  def account_type_name
    case self.account_type
    when 0
      I18n.t('root')
    when 1
      I18n.t('faculty')
    when 2
      I18n.t('preceptor')
    when 3
      I18n.t('student')
    else
      I18n.t('undefined')
    end
  end

  def admin?
    self.account_type == 0
  end

  def faculty?
    self.account_type == 1
  end

  def preceptor?
    self.account_type == 2
  end

  def student?
    self.account_type == 3
  end

  def has_power?
    self.account_type == 1 || self.account_type == 0
  end
end
