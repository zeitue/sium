class Location < ActiveRecord::Base
  has_many :events
  validates_presence_of :name
  validate :address_line
  validates_presence_of :country
  validates_presence_of :state
  validates_presence_of :city
  validates_presence_of :zip
  def identifier
    buffer = ""
    buffer += self.name.to_s + ' @ ' unless self.name.to_s.empty?
    buffer += self.address_line1.to_s + ' ' unless self.address_line1.to_s.empty?
    buffer += self.address_line2.to_s + ' ' unless self.address_line2.to_s.empty?
    buffer += country_name + ' ' unless self.country.to_s.empty?
    buffer += self.city.to_s + ', ' unless city.to_s.empty?
    buffer += self.state.to_s + ' ' unless self.state.to_s.empty?
    buffer += self.zip.to_s unless self.zip.to_s.empty?
    buffer += ', ' + self.clinical_site.to_s unless self.clinical_site.to_s.empty?
    buffer += ', ' + self.unit.to_s unless self.unit.to_s.empty?
    buffer
  end

  def googlemaps
    buffer = "https://www.google.com/maps/place/"
    buffer += self.name.to_s + ' , ' unless self.name.to_s.empty?
    buffer += self.address_line1.to_s + ' ' unless self.address_line1.to_s.empty?
    buffer += self.address_line2.to_s + ' ' unless self.address_line2.to_s.empty?
    buffer += country_name + ' ' unless self.country.to_s.empty?
    buffer += self.city.to_s + ', ' unless city.to_s.empty?
    buffer += self.state.to_s + ' ' unless self.state.to_s.empty?
    buffer += self.zip.to_s unless self.zip.to_s.empty?
    buffer
  end

  def address_line
    unless !address_line1.empty? or !address_line2.empty?
      errors.add(:base, "At least one address line must be supplied")
    end
  end

  def country_name
    country = ISO3166::Country[self.country.to_s]
    country.translations[I18n.locale.to_s] || country.name
  end
end
