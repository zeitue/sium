class TimeCourse < ActiveRecord::Base
  belongs_to :course, foreign_key: 'course_id', class_name: 'DefinedCourse'
  belongs_to :instructor, foreign_key: 'instructor_id', class_name: 'User'
  validates_presence_of :instructor
  validates_presence_of :course

  def identifier
    self.course.number.to_s + ' - ' + self.instructor.name
  end

  def full_id
    self.course.identifier + ' - ' + self.instructor.name
  end
end
