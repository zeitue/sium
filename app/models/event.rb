class Event < ActiveRecord::Base
  belongs_to :overseer, foreign_key: 'overseer_id', class_name: 'User'
  belongs_to :student, foreign_key: 'student_id', class_name: 'User'
  belongs_to :course, foreign_key: 'course_id', class_name: 'TimeCourse'
  belongs_to :location
  has_one :overseer_evaluation_form
  has_one :student_evaluation_form
  validates_presence_of :student
  validates_presence_of :overseer
  validates_presence_of :location
  validates_presence_of :course
  validates_presence_of :start
  validates_presence_of :stop

  def ended?
    !login.nil? && !logout.nil? && stop > DateTime.now
  end
end
