class Link < ActiveRecord::Base
  validates_presence_of :location
  validates_presence_of :name
  validates_presence_of :account_type
  validates_presence_of :protocol
  before_validation :default_values


  def default_values
    self.name = self.name.to_s.strip
    self.location = self.location.to_s.strip.sub(/^https?\:\/\//, '').sub(/ftp?\:\/\//,'').sub(/file?\:\/\//,'').sub(/^www./,'')
  end

  def link_button
    "<a target='_blank' class='btn btn-lg btn-block' href='#{(self.protocol_string + '://' + self.location.to_s)}'>#{self.name}</a>".html_safe
  end


  def service
  end

  def protocol_string
    case self.protocol
    when 0
      'file'
    when 1
      'http'
    when 2
      'https'
    when 3
      'ftp'
    when 4
    end
  end
end
