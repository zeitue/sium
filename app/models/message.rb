class Message < ActiveRecord::Base
  belongs_to :user
  belongs_to :to, :class_name => 'User'
  belongs_to :from, :class_name => 'User'
end
