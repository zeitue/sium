class Skill < ActiveRecord::Base
  belongs_to :user
  belongs_to :defined_skill
  validates_uniqueness_of :defined_skill, :scope => :user
  validates_presence_of :defined_skill
  validates_presence_of :user

end
