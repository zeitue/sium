class Contact < ActiveRecord::Base
  validates_presence_of :field
  belongs_to :user
end
