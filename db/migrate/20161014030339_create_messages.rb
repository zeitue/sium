class CreateMessages < ActiveRecord::Migration[4.2]
  def change
    create_table :messages do |t|
      t.string :subject
      t.text :body
      t.references :user, index: true, foreign_key: true
      t.references :to, index: true
      t.references :from, index: true

      t.timestamps null: false
    end
  end
end
