class CreateSkills < ActiveRecord::Migration[4.2]
  def change
    create_table :skills do |t|
      t.references :user, index: true, foreign_key: true
      t.references :defined_skill, index: true, foreign_key: true
      t.integer :level

      t.timestamps null: false
    end
  end
end
