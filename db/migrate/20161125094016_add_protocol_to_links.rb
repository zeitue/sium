class AddProtocolToLinks < ActiveRecord::Migration[4.2]
  def change
    add_column :links, :protocol, :integer
  end
end
