class CreateEvents < ActiveRecord::Migration[4.2]
  def change
    create_table :events do |t|
      t.references :overseer, index: true
      t.references :student, index: true
      t.references :course, index: true
      t.datetime :start
      t.datetime :stop
      t.datetime :login
      t.datetime :logout

      t.timestamps null: false
    end
  end
end
