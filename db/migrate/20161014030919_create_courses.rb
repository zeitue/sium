class CreateCourses < ActiveRecord::Migration[4.2]
  def change
    create_table :courses do |t|
      t.boolean :completed
      t.references :course, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
