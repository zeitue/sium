class CreateTimeCourses < ActiveRecord::Migration[4.2]
  def change
    create_table :time_courses do |t|
      t.references :course, index: true
      t.references :instructor, index: true
      t.datetime :start
      t.datetime :stop

      t.timestamps null: false
    end
  end
end
