class CreateDefinedSkills < ActiveRecord::Migration[4.2]
  def change
    create_table :defined_skills do |t|
      t.string :name
      t.string :category
      t.integer :required_level

      t.timestamps null: false
    end
  end
end
