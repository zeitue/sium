class AddDefaultValues < ActiveRecord::Migration[4.2]
  def up
    change_column :users, :prefix, :string, :default => ""
    change_column :users, :first_name, :string, :default => ""
    change_column :users, :middle_name, :string, :default => ""
    change_column :users, :last_name, :string, :default => ""
    change_column :users, :suffix, :string, :default => ""
    change_column :users, :pin, :integer, :default => 0
    change_column :users, :level, :integer, :default => 1
    change_column :users, :account_type, :integer, :default => 3
    change_column :users, :active, :boolean, :default => false
    change_column :users, :privacy, :boolean, :default => false
    change_column :users, :home_unit, :string, :default => ""

    change_column :programs, :name, :string, :default => ""

    change_column :notes, :title, :string, :default => ""
    change_column :notes, :body, :text, :default => ""
    change_column :notes, :background_color, :string, :default => "#ffffff"

    change_column :messages, :subject, :string, :default => ""
    change_column :messages, :body, :text, :default => ""

    change_column :locations, :name, :string, :default => ""
    change_column :locations, :address_line1, :string, :default => ""
    change_column :locations, :address_line2, :string, :default => ""
    change_column :locations, :country, :string, :default => ""
    change_column :locations, :state, :string, :default => ""
    change_column :locations, :city, :string, :default => ""
    change_column :locations, :zip, :string, :default => ""
    change_column :locations, :clinical_site, :string, :default => ""
    change_column :locations, :unit, :string, :default => ""

    change_column :contacts, :field, :string, :default => ""
    change_column :contacts, :type, :integer, :default => 0

    change_column :courses, :completed, :boolean, :default => false

    change_column :defined_courses, :name, :string, :default => ""
    change_column :defined_courses, :number, :string, :default => ""
    change_column :defined_courses, :level, :integer, :default => 1

    change_column :defined_skills, :name, :string, :default => ""
    change_column :defined_skills, :category, :string, :default => ""
    change_column :defined_skills, :required_level, :integer, :default => 1
  end

  def down
    change_column :users, :prefix, :string, :default => nil
    change_column :users, :first_name, :string, :default => nil
    change_column :users, :middle_name, :string, :default => nil
    change_column :users, :last_name, :string, :default => nil
    change_column :users, :suffix, :string, :default => nil
    change_column :users, :pin, :integer, :default => 0
    change_column :users, :level, :integer, :default => 0
    change_column :users, :account_type, :integer, :default => 0
    change_column :users, :active, :boolean, :default => nil
    change_column :users, :privacy, :boolean, :default => nil
    change_column :users, :home_unit, :string, :default => nil

    change_column :programs, :name, :string, :default => nil

    change_column :notes, :title, :string, :default => nil
    change_column :notes, :body, :text, :default => nil
    change_column :notes, :background_color, :string, :default => nil

    change_column :messages, :subject, :string, :default => nil
    change_column :messages, :body, :text, :default => nil

    change_column :locations, :name, :string, :default => nil
    change_column :locations, :address_line1, :string, :default => nil
    change_column :locations, :address_line2, :string, :default => nil
    change_column :locations, :country, :string, :default => nil
    change_column :locations, :state, :string, :default => nil
    change_column :locations, :city, :string, :default => nil
    change_column :locations, :zip, :string, :default => nil
    change_column :locations, :clinical_site, :string, :default => nil
    change_column :locations, :unit, :string, :default => nil

    change_column :contacts, :field, :string, :default => nil
    change_column :contacts, :type, :integer, :default => 0

    change_column :courses, :completed, :boolean, :default => nil

    change_column :defined_courses, :name, :string, :default => nil
    change_column :defined_courses, :number, :string, :default => nil
    change_column :defined_courses, :level, :integer, :default => 0

    change_column :defined_skills, :name, :string, :default => nil
    change_column :defined_skills, :category, :string, :default => nil
    change_column :defined_skills, :required_level, :integer, :default => 0
  end
end
