class CreateOverseerEvaluationForms < ActiveRecord::Migration[4.2]
  def change
    create_table :overseer_evaluation_forms do |t|
      t.references :event, index: true, foreign_key: true
      t.references :overseer, index: true
      t.integer :f0
      t.integer :f1
      t.integer :f2
      t.integer :f3
      t.integer :f4
      t.integer :f5
      t.integer :f6
      t.integer :f7
      t.text :comment

      t.timestamps null: false
    end
  end
end
