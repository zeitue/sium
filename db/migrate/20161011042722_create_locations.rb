class CreateLocations < ActiveRecord::Migration[4.2]
  def change
    create_table :locations do |t|
      t.string :name
      t.string :address_line1
      t.string :address_line2
      t.string :country
      t.string :state
      t.string :city
      t.string :zip
      t.string :clinical_site
      t.string :unit

      t.timestamps null: false
    end
  end
end
