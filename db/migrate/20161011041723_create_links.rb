class CreateLinks < ActiveRecord::Migration[4.2]
  def change
    create_table :links do |t|
      t.string :location
      t.string :name
      t.integer :account_type

      t.timestamps null: false
    end
  end
end
