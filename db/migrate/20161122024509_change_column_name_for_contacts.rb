class ChangeColumnNameForContacts < ActiveRecord::Migration[4.2]
  def change
    rename_column :contacts, :type, :type_field
  end
end
