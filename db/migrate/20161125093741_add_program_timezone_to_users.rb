class AddProgramTimezoneToUsers < ActiveRecord::Migration[4.2]
  def change
    add_reference :users, :program, index: true, foreign_key: true
    add_column :users, :timezone, :string
  end
end
