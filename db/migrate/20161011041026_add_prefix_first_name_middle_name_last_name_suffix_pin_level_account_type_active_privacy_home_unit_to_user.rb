class AddPrefixFirstNameMiddleNameLastNameSuffixPinLevelAccountTypeActivePrivacyHomeUnitToUser < ActiveRecord::Migration[4.2]
  def change
    add_column :users, :prefix, :string
    add_column :users, :first_name, :string
    add_column :users, :middle_name, :string
    add_column :users, :last_name, :string
    add_column :users, :suffix, :string
    add_column :users, :pin, :integer
    add_column :users, :level, :integer
    add_column :users, :account_type, :integer
    add_column :users, :active, :boolean
    add_column :users, :privacy, :boolean
    add_column :users, :home_unit, :string
  end
end
