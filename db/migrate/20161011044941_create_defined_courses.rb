class CreateDefinedCourses < ActiveRecord::Migration[4.2]
  def change
    create_table :defined_courses do |t|
      t.string :name
      t.string :number
      t.integer :level

      t.timestamps null: false
    end
  end
end
