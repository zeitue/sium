class DropMessages < ActiveRecord::Migration[4.2]
  def change
    drop_table :messages
  end
end
