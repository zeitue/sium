class AddBackgroundColorToNotes < ActiveRecord::Migration[4.2]
  def change
    add_column :notes, :background_color, :string
  end
end
