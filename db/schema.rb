# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161202021907) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "contacts", id: :serial, force: :cascade do |t|
    t.integer "type_field", default: 0
    t.string "field", default: ""
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_contacts_on_user_id"
  end

  create_table "courses", id: :serial, force: :cascade do |t|
    t.boolean "completed", default: false
    t.integer "course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_courses_on_course_id"
  end

  create_table "defined_courses", id: :serial, force: :cascade do |t|
    t.string "name", default: ""
    t.string "number", default: ""
    t.integer "level", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "defined_skills", id: :serial, force: :cascade do |t|
    t.string "name", default: ""
    t.string "category", default: ""
    t.integer "required_level", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "events", id: :serial, force: :cascade do |t|
    t.integer "overseer_id"
    t.integer "student_id"
    t.integer "course_id"
    t.datetime "start"
    t.datetime "stop"
    t.datetime "login"
    t.datetime "logout"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "location_id"
    t.index ["course_id"], name: "index_events_on_course_id"
    t.index ["location_id"], name: "index_events_on_location_id"
    t.index ["overseer_id"], name: "index_events_on_overseer_id"
    t.index ["student_id"], name: "index_events_on_student_id"
  end

  create_table "links", id: :serial, force: :cascade do |t|
    t.string "location"
    t.string "name"
    t.integer "account_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "protocol"
  end

  create_table "locations", id: :serial, force: :cascade do |t|
    t.string "name", default: ""
    t.string "address_line1", default: ""
    t.string "address_line2", default: ""
    t.string "country", default: ""
    t.string "state", default: ""
    t.string "city", default: ""
    t.string "zip", default: ""
    t.string "clinical_site", default: ""
    t.string "unit", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mailboxer_conversation_opt_outs", id: :serial, force: :cascade do |t|
    t.string "unsubscriber_type"
    t.integer "unsubscriber_id"
    t.integer "conversation_id"
    t.index ["conversation_id"], name: "index_mailboxer_conversation_opt_outs_on_conversation_id"
    t.index ["unsubscriber_id", "unsubscriber_type"], name: "index_mailboxer_conversation_opt_outs_on_unsubscriber_id_type"
  end

  create_table "mailboxer_conversations", id: :serial, force: :cascade do |t|
    t.string "subject", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mailboxer_notifications", id: :serial, force: :cascade do |t|
    t.string "type"
    t.text "body"
    t.string "subject", default: ""
    t.string "sender_type"
    t.integer "sender_id"
    t.integer "conversation_id"
    t.boolean "draft", default: false
    t.string "notification_code"
    t.string "notified_object_type"
    t.integer "notified_object_id"
    t.string "attachment"
    t.datetime "updated_at", null: false
    t.datetime "created_at", null: false
    t.boolean "global", default: false
    t.datetime "expires"
    t.index ["conversation_id"], name: "index_mailboxer_notifications_on_conversation_id"
    t.index ["notified_object_id", "notified_object_type"], name: "index_mailboxer_notifications_on_notified_object_id_and_type"
    t.index ["sender_id", "sender_type"], name: "index_mailboxer_notifications_on_sender_id_and_sender_type"
    t.index ["type"], name: "index_mailboxer_notifications_on_type"
  end

  create_table "mailboxer_receipts", id: :serial, force: :cascade do |t|
    t.string "receiver_type"
    t.integer "receiver_id"
    t.integer "notification_id", null: false
    t.boolean "is_read", default: false
    t.boolean "trashed", default: false
    t.boolean "deleted", default: false
    t.string "mailbox_type", limit: 25
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_delivered", default: false
    t.string "delivery_method"
    t.string "message_id"
    t.index ["notification_id"], name: "index_mailboxer_receipts_on_notification_id"
    t.index ["receiver_id", "receiver_type"], name: "index_mailboxer_receipts_on_receiver_id_and_receiver_type"
  end

  create_table "notes", id: :serial, force: :cascade do |t|
    t.string "title", default: ""
    t.text "body", default: ""
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "background_color", default: "#ffffff"
    t.index ["user_id"], name: "index_notes_on_user_id"
  end

  create_table "overseer_evaluation_forms", id: :serial, force: :cascade do |t|
    t.integer "event_id"
    t.integer "overseer_id"
    t.integer "f0"
    t.integer "f1"
    t.integer "f2"
    t.integer "f3"
    t.integer "f4"
    t.integer "f5"
    t.integer "f6"
    t.integer "f7"
    t.text "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_overseer_evaluation_forms_on_event_id"
    t.index ["overseer_id"], name: "index_overseer_evaluation_forms_on_overseer_id"
  end

  create_table "programs", id: :serial, force: :cascade do |t|
    t.string "name", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "skills", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "defined_skill_id"
    t.integer "level"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["defined_skill_id"], name: "index_skills_on_defined_skill_id"
    t.index ["user_id"], name: "index_skills_on_user_id"
  end

  create_table "student_evaluation_forms", id: :serial, force: :cascade do |t|
    t.integer "event_id"
    t.integer "student_id"
    t.integer "f0"
    t.integer "f1"
    t.integer "f2"
    t.integer "f3"
    t.integer "f4"
    t.integer "f5"
    t.integer "f6"
    t.text "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_student_evaluation_forms_on_event_id"
    t.index ["student_id"], name: "index_student_evaluation_forms_on_student_id"
  end

  create_table "time_courses", id: :serial, force: :cascade do |t|
    t.integer "course_id"
    t.integer "instructor_id"
    t.datetime "start"
    t.datetime "stop"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_time_courses_on_course_id"
    t.index ["instructor_id"], name: "index_time_courses_on_instructor_id"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "prefix", default: ""
    t.string "first_name", default: ""
    t.string "middle_name", default: ""
    t.string "last_name", default: ""
    t.string "suffix", default: ""
    t.integer "pin", default: 0
    t.integer "level", default: 1
    t.integer "account_type", default: 3
    t.boolean "active", default: false
    t.boolean "privacy", default: false
    t.string "home_unit", default: ""
    t.string "language", default: "en"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.integer "program_id"
    t.string "timezone"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["program_id"], name: "index_users_on_program_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  add_foreign_key "contacts", "users"
  add_foreign_key "courses", "courses"
  add_foreign_key "events", "locations"
  add_foreign_key "mailboxer_conversation_opt_outs", "mailboxer_conversations", column: "conversation_id", name: "mb_opt_outs_on_conversations_id"
  add_foreign_key "mailboxer_notifications", "mailboxer_conversations", column: "conversation_id", name: "notifications_on_conversation_id"
  add_foreign_key "mailboxer_receipts", "mailboxer_notifications", column: "notification_id", name: "receipts_on_notification_id"
  add_foreign_key "notes", "users"
  add_foreign_key "overseer_evaluation_forms", "events"
  add_foreign_key "skills", "defined_skills"
  add_foreign_key "skills", "users"
  add_foreign_key "student_evaluation_forms", "events"
  add_foreign_key "users", "programs"
end
