require 'test_helper'

class StudentEvaluationFormsControllerTest < ActionController::TestCase
  setup do
    @student_evaluation_form = student_evaluation_forms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:student_evaluation_forms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create student_evaluation_form" do
    assert_difference('StudentEvaluationForm.count') do
      post :create, student_evaluation_form: { comment: @student_evaluation_form.comment, event_id: @student_evaluation_form.event_id, f0: @student_evaluation_form.f0, f1: @student_evaluation_form.f1, f2: @student_evaluation_form.f2, f3: @student_evaluation_form.f3, f4: @student_evaluation_form.f4, f5: @student_evaluation_form.f5, f6: @student_evaluation_form.f6, student_id: @student_evaluation_form.student_id }
    end

    assert_redirected_to student_evaluation_form_path(assigns(:student_evaluation_form))
  end

  test "should show student_evaluation_form" do
    get :show, id: @student_evaluation_form
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @student_evaluation_form
    assert_response :success
  end

  test "should update student_evaluation_form" do
    patch :update, id: @student_evaluation_form, student_evaluation_form: { comment: @student_evaluation_form.comment, event_id: @student_evaluation_form.event_id, f0: @student_evaluation_form.f0, f1: @student_evaluation_form.f1, f2: @student_evaluation_form.f2, f3: @student_evaluation_form.f3, f4: @student_evaluation_form.f4, f5: @student_evaluation_form.f5, f6: @student_evaluation_form.f6, student_id: @student_evaluation_form.student_id }
    assert_redirected_to student_evaluation_form_path(assigns(:student_evaluation_form))
  end

  test "should destroy student_evaluation_form" do
    assert_difference('StudentEvaluationForm.count', -1) do
      delete :destroy, id: @student_evaluation_form
    end

    assert_redirected_to student_evaluation_forms_path
  end
end
