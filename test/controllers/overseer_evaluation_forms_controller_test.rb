require 'test_helper'

class OverseerEvaluationFormsControllerTest < ActionController::TestCase
  setup do
    @overseer_evaluation_form = overseer_evaluation_forms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:overseer_evaluation_forms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create overseer_evaluation_form" do
    assert_difference('OverseerEvaluationForm.count') do
      post :create, overseer_evaluation_form: { comment: @overseer_evaluation_form.comment, event_id: @overseer_evaluation_form.event_id, f0: @overseer_evaluation_form.f0, f1: @overseer_evaluation_form.f1, f2: @overseer_evaluation_form.f2, f3: @overseer_evaluation_form.f3, f4: @overseer_evaluation_form.f4, f5: @overseer_evaluation_form.f5, f6: @overseer_evaluation_form.f6, f7: @overseer_evaluation_form.f7, overseer_id: @overseer_evaluation_form.overseer_id }
    end

    assert_redirected_to overseer_evaluation_form_path(assigns(:overseer_evaluation_form))
  end

  test "should show overseer_evaluation_form" do
    get :show, id: @overseer_evaluation_form
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @overseer_evaluation_form
    assert_response :success
  end

  test "should update overseer_evaluation_form" do
    patch :update, id: @overseer_evaluation_form, overseer_evaluation_form: { comment: @overseer_evaluation_form.comment, event_id: @overseer_evaluation_form.event_id, f0: @overseer_evaluation_form.f0, f1: @overseer_evaluation_form.f1, f2: @overseer_evaluation_form.f2, f3: @overseer_evaluation_form.f3, f4: @overseer_evaluation_form.f4, f5: @overseer_evaluation_form.f5, f6: @overseer_evaluation_form.f6, f7: @overseer_evaluation_form.f7, overseer_id: @overseer_evaluation_form.overseer_id }
    assert_redirected_to overseer_evaluation_form_path(assigns(:overseer_evaluation_form))
  end

  test "should destroy overseer_evaluation_form" do
    assert_difference('OverseerEvaluationForm.count', -1) do
      delete :destroy, id: @overseer_evaluation_form
    end

    assert_redirected_to overseer_evaluation_forms_path
  end
end
