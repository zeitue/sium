require 'test_helper'

class TimeCoursesControllerTest < ActionController::TestCase
  setup do
    @time_course = time_courses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:time_courses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create time_course" do
    assert_difference('TimeCourse.count') do
      post :create, time_course: { course_id: @time_course.course_id, instructor_id: @time_course.instructor_id, start: @time_course.start, stop: @time_course.stop }
    end

    assert_redirected_to time_course_path(assigns(:time_course))
  end

  test "should show time_course" do
    get :show, id: @time_course
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @time_course
    assert_response :success
  end

  test "should update time_course" do
    patch :update, id: @time_course, time_course: { course_id: @time_course.course_id, instructor_id: @time_course.instructor_id, start: @time_course.start, stop: @time_course.stop }
    assert_redirected_to time_course_path(assigns(:time_course))
  end

  test "should destroy time_course" do
    assert_difference('TimeCourse.count', -1) do
      delete :destroy, id: @time_course
    end

    assert_redirected_to time_courses_path
  end
end
