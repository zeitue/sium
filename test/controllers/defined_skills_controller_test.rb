require 'test_helper'

class DefinedSkillsControllerTest < ActionController::TestCase
  setup do
    @defined_skill = defined_skills(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:defined_skills)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create defined_skill" do
    assert_difference('DefinedSkill.count') do
      post :create, defined_skill: { category: @defined_skill.category, name: @defined_skill.name, required_level: @defined_skill.required_level }
    end

    assert_redirected_to defined_skill_path(assigns(:defined_skill))
  end

  test "should show defined_skill" do
    get :show, id: @defined_skill
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @defined_skill
    assert_response :success
  end

  test "should update defined_skill" do
    patch :update, id: @defined_skill, defined_skill: { category: @defined_skill.category, name: @defined_skill.name, required_level: @defined_skill.required_level }
    assert_redirected_to defined_skill_path(assigns(:defined_skill))
  end

  test "should destroy defined_skill" do
    assert_difference('DefinedSkill.count', -1) do
      delete :destroy, id: @defined_skill
    end

    assert_redirected_to defined_skills_path
  end
end
