require 'test_helper'

class DefinedCoursesControllerTest < ActionController::TestCase
  setup do
    @defined_course = defined_courses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:defined_courses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create defined_course" do
    assert_difference('DefinedCourse.count') do
      post :create, defined_course: { level: @defined_course.level, name: @defined_course.name, number: @defined_course.number }
    end

    assert_redirected_to defined_course_path(assigns(:defined_course))
  end

  test "should show defined_course" do
    get :show, id: @defined_course
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @defined_course
    assert_response :success
  end

  test "should update defined_course" do
    patch :update, id: @defined_course, defined_course: { level: @defined_course.level, name: @defined_course.name, number: @defined_course.number }
    assert_redirected_to defined_course_path(assigns(:defined_course))
  end

  test "should destroy defined_course" do
    assert_difference('DefinedCourse.count', -1) do
      delete :destroy, id: @defined_course
    end

    assert_redirected_to defined_courses_path
  end
end
