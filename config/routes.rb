Rails.application.routes.draw do


  resources :student_evaluation_forms
  resources :overseer_evaluation_forms
  get 'dashboard' => 'dashboard#index'
  get 'event/calendar' => 'events#calendar'
  get 'master_clinical_log' => 'events#master_clinical_log'
  get 'event/list' => 'events#list'
  get 'event/todo' => 'events#todo'
  get 'events/:id/login'  => 'events#login', as: :events_login
  patch 'events/:id/login'  => 'events#login_update', as: :events_login_update
  get 'events/:id/logout'  => 'events#logout', as: :events_logout
  patch 'events/:id/logout'  => 'events#logout_update', as: :events_logout_update
  get 'skills/:id/level_up'  => 'skills#level_up', as: :skills_level_up
  patch 'skills/:id/level_up'  => 'skills#level_up_update', as: :skills_level_up_update

  resources :conversations, only: [:index, :show, :destroy] do
    member do
      post :reply
      post :restore
      post :mark_as_read
      post :mark_as_unread
    end
    collection do
      delete :empty_trash
    end
  end
  resources :messages, only: [:new, :create]


  match "/event/calendar/:year/:month", :to => "events#calendar", via: [:get, :post],
        :constraints => { :year => /\d{4}/, :month => /\d{1,2}/ },
        :as => 'events_calendar'


  match "/event/list/:year/:month/:day", :to => "events#list", via: [:get, :post],
        :constraints => { :year => /\d{4}/, :month => /\d{1,2}/ , :day => /\d{1,2}/},
        :as => 'events_list_day'

  match "/event/list/:year/:month", :to => "events#list", via: [:get, :post],
        :constraints => { :year => /\d{4}/, :month => /\d{1,2}/ },
        :as => 'events_list_month'

  match "/event/list/:year", :to => "events#list", via: [:get, :post],
        :constraints => { :year => /\d{4}/},
        :as => 'events_list_year'
  resources :courses
  resources :time_courses
  resources :programs
  resources :events
  resources :defined_courses
  resources :contacts, path: '/profile/contacts'
  resources :skills
  resources :defined_skills
  resources :locations  do
    get :autocomplete, :on => :collection
  end
  resources :links
  devise_for :users, path: '/profile', :controllers => {:registrations => "registrations"}
  resources :users do
    get :autocomplete, :on => :collection
    get :autocomplete_student, :on => :collection
    get :autocomplete_overseer, :on => :collection
    get :autocomplete_faculty, :on => :collection
  end

  resources :notes do
    member do
      get 'download'
    end
  end
  devise_scope :user do
    authenticated :user do
      root 'events#calendar', as: :authenticated_root
      root :to => 'events#calendar'

    end
    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
